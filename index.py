import qrcode
import qrcode.image.svg
import bottle
from bottle import post, get, request
import os
import random
import xml.etree.ElementTree as ET
from bs4 import BeautifulSoup
from urllib.parse import parse_qs

IMG_LOC = "images/code.svg"
STATIC_LOC = os.getcwd()
VALID_CHARS = "ABCDEFGHIJKLMNPQRSTUVWXYZ123456789"


def new_qr_img(code, loc=IMG_LOC, err="M"):

    if err == "H":
        correction = qrcode.constants.ERROR_CORRECT_H
    else:
        correction = qrcode.constants.ERROR_CORRECT_M

    QR = qrcode.QRCode(
        version=1,
        error_correction=correction,
        box_size=90,
        border=0,
        image_factory=qrcode.image.svg.SvgPathImage,
    )

    QR.add_data(code)

    img = QR.make_image(fill_color="black", back_color="white")

    # img.save(loc)

    return f"""<svg width="270" height="270" version="1.1" viewBox="0 0 270 270" xmlns="http://www.w3.org/2000/svg">{ET.tostring(img.make_path())}</svg>"""


app = bottle.Bottle(__name__)


@app.route("/")
def favicon():
    return "Hello!"


@app.route("/code")
@app.route("/code/")
@app.route("/code/<num:int>")
def hello(num=5):
    code = "".join(random.choices(VALID_CHARS, k=int(num)))
    yield code
    # TODO: Generate qr_image preemptively
    # new_qr_img(code, loc=f"images/{code}.svg")


@app.route("/img")
@app.route("/img/<err>")
def code_gen(err="M"):
    # TODO: Attempt to load preemptively image from disk

    code = request.query.code

    if 1 > len(code):
        return "Try adding `?code={something}` to the URL "

    qr_code = new_qr_img(code, err=err)

    return qr_code


@app.route("/mashup", method="POST")
def mash_up():

    file = request.files.get("qr")
    if not file:
        qr = parse_qs(request.body.getvalue().decode("utf-8"))["qr"][0]

    else:
        qr = file.file.read()

    qr_soup = BeautifulSoup(qr, "xml")

    temp = request.files.get("temp")
    if temp:
        # print("Received template file, saving to images/temp.svg")

        temp.save(f"images/temp.svg")
        soup = BeautifulSoup(temp.file.read(), "xml")
    else:
        # print("No template given, using stored template")

        with open("images/temp.svg") as fp:
            soup = BeautifulSoup(fp, "xml")

    path = soup.find(id="qr-path")

    path["d"] = qr_soup.svg.path["d"]

    return str(soup)


# TODO: Reset function
# def reset():
#     pass
# @app.route("/reset/")
# def reset_route():
#     reset()
#     return "Hello"


if __name__ == "__main__":
    # reset()
    bottle.run(app, reloader=True, debug=True, port=8080)

# python3.8 -m pip install -r requirements.txt ; python3.8 -m bottle main
