# QR Code API
### For all your QR Code generation needs

## To get started:
* Clone this repo to your local machine. Try: ```git clone https://gitlab.com/C1ARKGABLE/qr-code-gen```
* Move into the newly created directory. Try: ```cd qr-code-gen```
* Install the Python requirements. These are stored in 'requirements.txt', but pip3 can read them with: ```pip3 install -r requirements.txt```
* Run the web service with: ```python3 index.py```
* Congrats, you've successfully started the web api

## Use:
* Visit http://localhost:8080/, and you should see "Hello!"
* Now visit http://localhost:8080/code, you should see the default length (5) random string. (Hint: if you go to the route code/(number), it will generate a random string with that length)
* With your new code, you can generate a QR-Code by going to http://localhost:8080/img?code={url+code}
* This will generate an SVG QR-Code and return it
* With your new QR-Code, you can embed it into a saved template svg image with a simple POST request. Try the following from the root of this project:
```curl -X POST -F 'qr=@images/code.svg' localhost:8080/mashup > new.svg ; open new.svg```
* If you'd like to embed the QR-code into a new template svg, you can add the svg file to the ```temp``` attribute of your POST request. Try the following:
```curl -X POST -F 'temp=@/path/to/template.svg' -F 'qr=@images/code.svg' localhost:8080/mashup > new.svg ; open new.svg```
* NOTE: This new svg template will be saved for future use. If you bork this, try: ```git restore images/temp.svg```
* To string all the commands together, try running ```./command.sh```