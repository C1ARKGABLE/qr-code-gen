import requests
import cairosvg
from PyPDF2 import PdfFileReader, PdfFileWriter
from io import BytesIO
from tqdm import tqdm
from bs4 import BeautifulSoup

n = 1_000
URL = f"http://tag.ws/i/"
codes = set()
writer = PdfFileWriter()
DUMMY_INTERVAL = 100


def get_unique_code(codes, n=5):
    """Get an n-length unique code, recursively"""
    result = requests.get(f"http://localhost:8080/code/{n}")

    code = result.content.decode("utf-8")

    if code in codes:
        print("Duplicate code, trying again")
        return get_unique_code(codes)

    return code


def get_qr(url_code, err="M"):
    """Make a QR code from a unique url and code"""
    result = requests.get(f"http://localhost:8080/img/{err}?code={url_code}")

    return result.content.decode("utf-8")


def get_final_svg(qr):
    """Get the final svg with a qr code"""
    result = requests.post(f"http://localhost:8080/mashup", data={"qr": qr})

    return result.content


def get_final_pdf(svg):
    """Convert the svg into a PdfFileReader object for easy writing"""
    pdf = BytesIO()
    cairosvg.svg2pdf(bytestring=svg, write_to=pdf)
    return PdfFileReader(pdf)


if __name__ == "__main__":
    with open("images/spacer.svg") as fp:
        spacer_xml = BeautifulSoup(fp, "xml")

    spacer_counter = spacer_xml.find(id="num-text")

    # Open this csv file to keep track of our codes
    with open("kilocode.csv", "w+") as file:
        # Write the header
        file.write("order,url,code,svg\n")

        # Do this n times, with a pretty loading bar
        for i in tqdm(range(n)):

            if i % DUMMY_INTERVAL == 0:
            if DUMMY_INTERVAL != -1 and i % DUMMY_INTERVAL == 0:

                spacer_pdf = get_final_pdf(str(spacer_xml))

                writer.addPage(spacer_pdf.getPage(0))

            # Get a unique code
            code = get_unique_code(codes)

            # Get that code's QR svg
            qr_svg = get_qr(f"{URL}{code}", err="H")

            # Set the QR svg into the template svg
            final_svg = get_final_svg(qr_svg)

            # Convert the svg into a pdf, then a file reader
            final_pdf = get_final_pdf(final_svg)

            # Write the pdf page to the writer
            writer.addPage(final_pdf.getPage(0))

            # Log everything, with proper CSV syntax
            codes.add(code)
            escaped = qr_svg.replace('"', '""')
            file.write(f'{i},{URL},{code},"{escaped}"\n')

    # Write the final pdf pages into one pdf
    with open("kilocode.pdf", "wb") as output_stream:
        writer.write(output_stream)
